import styled from 'styled-components'

export const DivStyle = styled.div`
    background-color: #051923;
    display: flex;
    justify-content: center;
    align-items: center;
    height: 100vh;
    > span {
    background: #5bff71;
    width: 15px;
    height: 50px;
    display: inline-block;
    margin: 0 2px;
    }
    span:nth-child(1){
        background-color: #f11e30;
        animation: grow 0.5s -0.45s ease-in-out alternate infinite;
    }
    span:nth-child(2){
        background-color: #f07911;
        animation: grow 0.5s -0.30s ease-in-out alternate infinite;
    }span:nth-child(3){
        background-color: #f2d70e;
        animation: grow 0.5s -0.15s ease-in-out alternate infinite;
    }span:nth-child(4){
        background-color: #43e31c;
        animation: grow 0.5s 0.0s ease-in-out alternate infinite;
    }span:nth-child(5){
        background-color: #378ad8;
        animation: grow 0.5s 0.15s ease-in-out alternate infinite;
    }
    span:nth-child(6){
        background-color: #b41dd9;
        animation: grow 0.5s 0.30s ease-in-out alternate infinite;
    }
    @keyframes grow{
        from{
            opacity: 0.2;
        }
        to{
            transform: scaleY(2)
        }
    }
`;